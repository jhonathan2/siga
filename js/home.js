$(document).ready(arranque);
var Usuario;
function arranque()
{
	if(!localStorage.wsp_siga)
	{window.location.replace("index.html");}

	Usuario = JSON.parse(localStorage.wsp_siga);

	$(".logout").click(cerrarSesion);
	funcionesMenu();

	$("#menu").show('slide');
	$("#Seccion_menu").hide('slide');
}
function Mensaje(titulo, texto, duracion)
{
	var id = "msg_" + obtenerPrefijo();
	var tds = '<div id="' + id + '" class="mensajes">';
	tds += '<a>x</a>';
	tds += '<strong>' + titulo + '</strong><br>';
	tds += '<small>' + texto + '</small>';
	tds += '</div>';
	$("body").append(tds);

	var msgs = $(".mensajes");
	var idxTop = 0;
	if (msgs.length > 1)
	{
		$.each(msgs, function(index, el) 
		{
			idxTop = idxTop + 30 + parseInt($(el).height()); 
		});
		
		$("#" + id).css("top", idxTop + "px");
	}
	

	$("#" + id + " a").click(function(event) 
	{
		$(this).parent("div").remove();
	});
	
	if (duracion === null)
	{
		duracion = 2600;
	}
	
	if (duracion != 0)
	{
		$("#" + id).fadeIn(300).delay(2600).fadeOut(600, function() 
		{
			$("#" + id).remove();
		});
	} else
	{
		$("#" + id).fadeIn(300);
	}
}
function validar(elemento)
{
	var obj = $(elemento + ' [required]');
	var bandera = true;
	$.each(obj, function(index, val) 
	{
		 if (($(val).prop("tagName") == "SELECT" && $(val).val() == 0) || $(val).val() == "")
		 {
		 	Mensaje("Hey", "El campo " + $(val).attr("placeholder") + " es Obligatorio");
		 	$(val).focus();
		 	bandera = false;
			return false;
		 }
	});
	return bandera;
}
function cerrarSesion()
{
	delete localStorage.wsp_siga;
	window.location.replace("index.html");
}
function botonMenu_Click(event)
{
	event.preventDefault();
	$("#Seccion_menu").show('slide');
	var id = "#" + $(this).attr("vinculo");
	var texto = $(this).find('.botonMenu_Texto').text();
	var imagen = $(this).find('.botonMenu_Imagen').find("img").attr("src");
	imagen = imagen.replace("css/flatIcons/svg/", "flaticon-");
	imagen = imagen.replace(".svg", "");
	$(".Seccion").hide();
	
		$("#Seccion_conf span").text(texto);
		$("#Seccion_conf i").removeClass();
		$("#Seccion_conf i").addClass(imagen);
		$(id).show('slide');
	
}
function funcionesMenu()
{
	$("#Seccion_menu").hover(function() 
	{
		if (!$("#menu").is(":visible"))
		{
			$("#menu").addClass('menuDialog');
			$("#menu").slideDown('fast');
		}
	}, function() {});

	$("#Seccion_menu").click(function(event) 
	{
		event.preventDefault();
		
		$(".Seccion").hide("slide");
		$("#menu").removeClass('menuDialog');
		$("#menu").show('slide');
			$("#Seccion_conf span").text("Inicio");
			$("#Seccion_conf i").removeClass();
			$("#Seccion_conf i").addClass("flaticon-public6");
		$("#Seccion_menu").hide("slide");

	});

	$("#menu").hover(function() {}, function() {
		if ($("#menu").hasClass('menuDialog'))
		{
			$("#menu").hide();
			$("#menu").removeClass('menuDialog');
		}
	});
	
	$(".botonMenu").click(botonMenu_Click);
}
function cargarRegionales(objeto)
{
	$(objeto).find("option").remove();
	$.post('php/cargarRegionales.php', {pUsuario : Usuario.id}, function(data, textStatus, xhr) 
	{
		var tds = "";
		$.each(data, function(index, val) 
		{
			 tds += "<option value='" + val.idRegional + "'>" + val.Nombre + "</option>";
		});
		$(objeto).append(tds);
	}, "json");
}
function cargarAreas(objeto)
{
	$(objeto).find("option").remove();
	$.post('php/cargarAreas.php', {pUsuario : Usuario.id}, function(data, textStatus, xhr) 
	{
		var tds = "";
		$.each(data, function(index, val) 
		{
			 tds += "<option value='" + val.idArea + "'>" + val.Nombre + "</option>";
		});
		$(objeto).append(tds);
	}, "json");
}
function cargarEstadosActivo(objeto)
{
	$(objeto).find("option").remove();
	$.post('php/cargarEstadosActivo.php', {pUsuario : Usuario.id}, function(data, textStatus, xhr) 
	{
		var tds = "";
		$.each(data, function(index, val) 
		{
			 tds += "<option value='" + val.idEstadoActivo + "'>" + val.Nombre + "</option>";
		});
		$(objeto).append(tds);
	}, "json");
}
function obtenerPrefijo()
{
  var f = new Date();
    return f.getFullYear() + CompletarConCero(f.getMonth() +1, 2) + CompletarConCero(f.getDate(), 2) + CompletarConCero(f.getHours(), 2) + CompletarConCero(f.getMinutes(), 2) + CompletarConCero(f.getSeconds(), 2) + CompletarConCero(Usuario.id, 4);
}
function CompletarConCero(n, length)
{
   n = n.toString();
   while(n.length < length) n = "0" + n;
   return n;
}
function obtenerFecha()
{
  var f = new Date();
    return f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2) + " " + CompletarConCero(f.getHours(), 2) + ":" + CompletarConCero(f.getMinutes(), 2) + ":" + CompletarConCero(f.getSeconds(), 2);
}
$.fn.reset = function(callback)
{
	$(this).find("input").val("");
	$(this).find("select").val(0);
	$(this).find("textarea").val("");

	if (callback === undefined)
    {callback = function(){};}

	callback.call();
}
$.fn.cargarResultados = function(datos, parametros, restricciones, atributos, callback)
{
	if (datos != 0)
	{
		if (callback === undefined)
	    {callback = function(){};}

		if (restricciones === undefined)
		{restricciones = {};}

		if (atributos === undefined)
		{atributos = {};}

		if (parametros === undefined || parametros === 0)
		{parametros = "CTW";}

		var idTabla = $(this).attr("id");

		if ($("#" + idTabla + "_wrapper").length == 1)
		{
			$(this).dataTable().fnDestroy();
		} 

		$(this).find('tbody').remove();
		$(this).find('thead').remove();

		var tds = "<thead><tr>";
		var Titulos = Object.keys(datos[0]);
		var bandera = true;

		$.each(Titulos, function(index, val) 
		{
			bandera = true;
			 $.each(restricciones, function(key, value) 
			 {
				if ((val + " ").search(value + " ") >= 0)
				{
					bandera = false;
				}
			 });
			 if (bandera)
			 {
				 tds += "<th>" + val + "</th>";
			 }
		});
		tds += "</tr></thead>";
		tds += "<tbody>";
		
		var tmpAtributos = "";

		$.each(datos, function(index, val) 
		{
				tmpAtributos = "";

			tds += "<tr";
			 $.each(val, function(key, value) 
			 {
				$.each(atributos, function(keyR, valueR) 
				 {
					if ((key + " ").search(valueR + " ") >= 0)
					{
						tmpAtributos += " " + key + "='" + value + "' ";
					}
				 });
				bandera = true;
				 $.each(restricciones, function(keyR, valueR) 
				 {
					if ((key + " ").search(valueR + " ") >= 0)
					{
						bandera = false;
					}
				 });
				 tds += tmpAtributos + ">";
				 tmpAtributos = "";
				 if (bandera)
				 {
					tds += "<td>" + value + "</td>";
				 }
			 });
			tds += "</tr>";
		});
		tds += "</tbody>";

		$(this).append(tds);
		

		$(this).dataTable( {
						"sDom": parametros + '<"clear">lfrtip',
						"aaSorting": [[ 0, "asc" ]],
						"oTableTools": 
							{
						"sSwfPath": "Tools/datatable/media/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
			                "print",
			                {
			                    "sExtends":    "collection",
			                    "sButtonText": "Guardar",
			                    "aButtons":    [ "csv", "xls", "pdf" ]
			                }
			            			]
							},
							"oColumnFilterWidgets": 
							{
								"sSeparator": "\\s*/+\\s*",
								"bGroupTerms" : true
							}
					} );
		
		var objFiltros = $("#" + idTabla + "_wrapper .column-filter-widget");

		$(objFiltros[2]).hide();

		callback.call();
	} else
	{
		Mensaje("Error", "No hay datos para mostrar");
	}
}
$.fn.generarDatosEnvio = function(restricciones, callback)
{
	if (callback === undefined)
    {callback = function(){};}

    var obj = $(this).find(".guardar");
	var datos = {};

	datos['Prefijo'] = $("txtPrefijo").val();
	$.each(obj, function(index, val) 
	{
		if ($(val).attr("id") != undefined)
		{
		 datos[$(val).attr("id").replace(restricciones, "")] = $(val).val();
		}
	});
	datos = JSON.stringify(datos);	

	callback(datos);
}