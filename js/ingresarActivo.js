$(document).ready(ingresarActivo);
function ingresarActivo()
{
	$( "#NuevoActivo_FechaIngreso" ).datepicker(
	{
      altField : "#txtNuevoActivo_FechaIngreso",
      changeMonth: true,
      changeYear: true,
      showOtherMonths: true
    });

    $("#NuevoActivo_FechaIngreso").click(function(event) 
    {
    	//$("#txtNuevoActivo_FechaIngreso").val($("#NuevoActivo_FechaIngreso").datepicker( "getDate" ));
    });

    cargarRegionales($("#txtNuevoActivo_Regional"));
    cargarAreas($("#txtNuevoActivo_Area"));
    cargarEstadosActivo($("#txtNuevoActivo_Estado"));
    $("#txtNuevoActivo_Cedula").change(txtNuevoActivo_Cedula_Change);

    $("#NuevoActivo_IngresarActivo").submit(NuevoActivo_IngresarActivo_Submit);

    $("#btnNuevoActivo_LimpiarCampos").click(function(event) 
    {
    	event.preventDefault();
    	nuevoActivo_Reset();
    });

    $("#btnAgregar").click(function(event) 
    {
    	event.preventDefault();

    });
}
function txtNuevoActivo_Cedula_Change()
{
	$.post('php/validarCedula.php', {usuario : Usuario.id, cedula : $("#txtNuevoActivo_Cedula").val()}, 
		function(data, textStatus, xhr) 
		{
			if (data != 0)
			{
				$("#txtNuevoActivo_Responsable").val(data.idLogin);
				$("#lblNuevoActivo_Responsable").text(data.Nombre + " de " + data.Area + " " + data.Regional);
			} else
			{
				$("#txtNuevoActivo_Responsable").val('');
				$("#lblNuevoActivo_Responsable").text('');
			}
		}, "json");
}
function NuevoActivo_IngresarActivo_Submit(evento)
{
	evento.preventDefault();

	if (validar("#NuevoActivo_IngresarActivo"))
	{
		$("#NuevoActivo_IngresarActivo").generarDatosEnvio("txtNuevoActivo_", function(datos)
			{
				$.post('php/crearActivo.php', {datos: datos}, 
					function(data, textStatus, xhr) 
					{
						if (parseInt(data) == 1)
						{
							Mensaje("Ok", "El Activo ha sido guardado");
							nuevoActivo_Reset();
						} else
						{
							Mensaje("Error", "No se puede ingresar el activo, por favor revise los parámetros e intente nuevamente");
						}
					}).fail(function()
					{
						Mensaje("Error", "Se perdió la conexión con el servidor");
					});
			});

	} else
	{
		console.log("paila");
	}
}
function nuevoActivo_Reset()
{
	$("#NuevoActivo_IngresarActivo").reset(function()
	{
		$("#lblNuevoActivo_Responsable").text('');
	});
}