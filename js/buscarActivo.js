$(document).ready(buscarActivo);
function buscarActivo()
{
	$("#btnBuscarActivos_BusquedaAvanzada").click(function(event) 
	{
		event.preventDefault();
		if ($("#BuscarActivos_BusquedaAvanzada").is(":visible"))
		{
			$("#BuscarActivos_BusquedaAvanzada").slideUp();
		} else
		{
			$("#BuscarActivos_BusquedaAvanzada").slideDown();
		}
	});

	$("#txtBusquedaAvanzada_FechaIngreso").datepicker(
	{
      changeMonth: true,
      changeYear: true,
      showOtherMonths: true
    });

	$("#BuscarActivos_Busqueda").submit(function(event) 
	{
		event.preventDefault();
    	$("#BuscarActivos_BusquedaAvanzada").slideUp();
    	$.post('php/cargarBusqueda.php', {parametro : $("#txtBuscarActivos_Parametro").val()}, function(data, textStatus, xhr) 
    	{
    		$("#tblBuscarActivos_Resultados").cargarResultados(data, 0, ["idActivo"], ["idActivo"]);

    	}, "json");
	});
    $("#BuscarActivos_BusquedaAvanzada").submit(function(event) 
    {
    	event.preventDefault();
    	$("#BuscarActivos_BusquedaAvanzada").generarDatosEnvio("txtBusquedaAvanzada_", function(datos)
    		{
		    	$("#BuscarActivos_BusquedaAvanzada").slideUp();
		    	$.post('php/cargarBusquedaAvanzada.php', {datos : datos}, function(data, textStatus, xhr) 
		    	{
		    		$("#tblBuscarActivos_Resultados").cargarResultados(data, 0, ["idActivo"], ["idActivo"]);

		    	}, "json");
    		});
    });

    $("#tblBuscarActivos_Resultados tbody tr").live("click", function(event) 
    {
    	$("#txtPrefijo").val($(this).attr("idActivo"));
    });
}